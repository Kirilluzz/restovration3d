using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraTransport : MonoBehaviour
{
    public Transform curTarget;
    public Transform curMoveTarget;

    public Transform target1;
    public Transform MoveTarget1;

    public Transform target2;
    public Transform MoveTarget2;
    public float speed = 5f;

    public float MoveSpeed = 5f;

    public GameObject backButton;


    private void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                RaycastHit hit;
                Ray ray = transform.GetComponent<Camera>().ScreenPointToRay(touch.position);

                if (Physics.Raycast(ray, out hit))
                {
                    Transform objectHit = hit.transform;

                    if(objectHit.tag == "PointToTeleport")
                    {
                        curTarget = target2;
                        curMoveTarget = MoveTarget2;
                        backButton.SetActive(true);
                    }
                }
            }
        }
        //������ ����� ��� ��������
        Vector3 direction = curTarget.transform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, speed * Time.deltaTime);
        //��� ��������
        Vector3 dir = curMoveTarget.position - transform.position;
        transform.Translate(dir.normalized * MoveSpeed * Time.deltaTime, Space.World);
    }

    public void makeBack()
    {
        curTarget = target1;
        curMoveTarget = MoveTarget1;
        backButton.SetActive(false);
    }
}
